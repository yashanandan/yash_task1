from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import false

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///temp_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = false

db = SQLAlchemy(app)

# Create our database model
class Temperature(db.Model):

    __tablename__ = "temperatures"

    id = db.Column(db.Integer, primary_key=True)
    location = db.Column(db.String(120))
    date = db.Column(db.String(50))
    temperature = db.Column(db.Integer)

    def __init__(self, location, date, temperature):
        self.location = location
        self.date = date
        self.temperature = temperature

    def __repr__(self):
        return '<Temperature %r>' % self.temperature

@app.route('/')
def index():
    print("POST - http://localhost/<LOCATION>/<DD/MM/YYYY>/<TEMPERATURE>  : To Store the temperature")
    print("GET - http://localhost/<LOCATION>/<DD/MM/YYYY>  : To get the temperature")
    return

@app.route('/<location>/<date>/<int:temperature>', methods=['POST'])
def insertTemperature(location, date, temperature):
    if request.method == 'POST':
        newTemp = Temperature(location, date, temperature)
        db.session.add(newTemp)
        db.session.commit()
        return "Successfully inserted"
    return "Failed to insert"

@app.route('/<location>/<date>', methods=['GET'])
def getTemperature(location, date):
    if request.method == 'GET':
        newTemp = Temperature.query.filter_by(location=location).filter_by(date=date).first()
        print(newTemp.temperature)
        formattedDate = date[0]+date[1] + "/" + date[2]+date[3] + "/" + date[4]+date[5]+date[6]+date[7]
    return "Temperature in "+ location + " on " + formattedDate + " is " + str(newTemp.temperature)

if __name__ == '__main__':
    app.debug = True
    app.run(host='localhost', port=1234)

